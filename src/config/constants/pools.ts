import { PoolConfig, QuoteToken, PoolCategory } from './types'

const pools: PoolConfig[] = [
  {
    pastaId: 0,
    tokenName: 'BREAD',
    earnToken: 'BREAD',
    stakingTokenName: QuoteToken.BREAD,
    stakingTokenAddress: '0x0e09fabb73bd3ade0a17ecc321fd13a19e81ce82',
    contractAddress: {
      97: '',
      56: '0xBA2fbA507fF19260E5AB1a30c00D284901F1E7F3',
    },
    poolCategory: PoolCategory.CORE,
    projectLink: 'https://breadswap.finance',
    harvest: false,
    tokenPerBlock: '1',
    sortOrder: 1,
    isFinished: false,
    tokenDecimals: 18,
  },

{
 pastaId: 3,
 tokenName: 'KP3RB',
 earnToken: 'KP3RB',
 stakingTokenName: QuoteToken.KP3RB,
 stakingTokenAddress: '0x0e09fabb73bd3ade0a17ecc321fd13a19e81ce82',    //
 contractAddress: {
   97: '',
   56: '0x84d20c47dbFd67DFb9449CAc8076c27889bEb365',
 },
 poolCategory: PoolCategory.CORE,
 projectLink: 'https://keep3rb.network/',
 harvest: true,
 tokenPerBlock: '0.40',
 sortOrder: 2,
 isFinished: false,
 tokenDecimals: 18,
},
{
 pastaId: 4,
 tokenName: 'CHS',
 earnToken: 'CHS',
 stakingTokenName: QuoteToken.CHS,
 stakingTokenAddress: '0x0e09fabb73bd3ade0a17ecc321fd13a19e81ce82',    //
 contractAddress: {
   97: '',
   56: '0x50256622Ed594239AFAf7D6aaC2A852aDfBD741c',
 },
 poolCategory: PoolCategory.CORE,
 projectLink: 'https://cheeseswap.app/',
 harvest: false,
 tokenPerBlock: '0.70',
 sortOrder: 3,
 isFinished: false,
 tokenDecimals: 18,
},
{
pastaId: 6,
tokenName: 'XCHS',
earnToken: 'CHS',
stakingTokenName: QuoteToken.XCHS,
stakingTokenAddress: '0x0e09fabb73bd3ade0a17ecc321fd13a19e81ce82',    //
contractAddress: {
  97: '',
  56: '0xfE5Ea18dafb4aB5Da2C212441f2C54A3C9676dD9',
},
poolCategory: PoolCategory.CORE,
projectLink: 'https://cheeseswap.app/',
harvest: false,
tokenPerBlock: '0.03',
sortOrder: 4,
isFinished: false,
tokenDecimals: 18,
},
{
 pastaId: 1,
 tokenName: 'CHS',
 earnToken: 'CHS',
 stakingTokenName: QuoteToken.CHS,
 stakingTokenAddress: '0x0e09fabb73bd3ade0a17ecc321fd13a19e81ce82',    //
 contractAddress: {
   97: '',
   56: '0xA647312aeD03a4D955D962E072e11032774D96Bf',
 },
 poolCategory: PoolCategory.CORE,
 projectLink: 'https://cheeseswap.app/',
 harvest: false,
 tokenPerBlock: '0.025',
 sortOrder: 5,
 isFinished: true,
 tokenDecimals: 18,
},
{
pastaId: 2,
tokenName: 'KP3RB',
earnToken: 'KP3RB',
stakingTokenName: QuoteToken.KP3RB,
stakingTokenAddress: '0x0e09fabb73bd3ade0a17ecc321fd13a19e81ce82',    //
contractAddress: {
  97: '',
  56: '0x65932e719B1108516B5d0A5cFdB6508A1268cAc2',
},
poolCategory: PoolCategory.CORE,
projectLink: 'https://keep3rb.network/',
harvest: false,
tokenPerBlock: '0.001',
sortOrder: 6,
isFinished: true,
tokenDecimals: 18,
},
{
pastaId: 5,
tokenName: 'HOTS',
earnToken: 'HOTS',
stakingTokenName: QuoteToken.HOTS,
stakingTokenAddress: '0x0e09fabb73bd3ade0a17ecc321fd13a19e81ce82',    //
contractAddress: {
  97: '',
  56: '0x7cAA197cdc8c6b878d5a44b3D0bcdFc7cc6AA66c',
},
poolCategory: PoolCategory.CORE,
projectLink: 'https://hotdog.cafe/',
harvest: false,
tokenPerBlock: '0.03',
sortOrder: 4,
isFinished: false,
tokenDecimals: 18,
},
]

export default pools
